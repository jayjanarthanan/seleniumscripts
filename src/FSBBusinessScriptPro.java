import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Random;

public class FSBBusinessScriptPro {

    public static void main(String[] args) throws InterruptedException {
        //Login in sequence begin
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Eclipse\\cHROMEdRIVER\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        int i;

        //used to generate random 6 digit string for end of SSN
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);

        //Set time stamp variable to use as first name
        String timeStamp = new SimpleDateFormat("yyy.MM.dd.HH.mm.ss").format(new java.util.Date());


        String encodedPassword = "put encoded password here";

        driver.get("https://test.salesforce.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.dev");
        encrypt.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        driver.findElement(By.xpath("//*[@id='Login']")).click();

        Thread.sleep(5000);

        driver.findElement(By.linkText("Customers")).click();

        driver.findElement(By.name("new")).click();
        driver.findElement(By.id("p3")).click();
        new Select(driver.findElement(By.id("p3"))).selectByVisibleText("Business");
        driver.findElement(By.id("p3")).click();
        driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).click();
        Thread.sleep(3000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).clear();
        Thread.sleep(1000);
        //Last Name
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).sendKeys(timeStamp);
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).clear();
        Thread.sleep(1000);
        //First Name
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).sendKeys("Business");
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).sendKeys("666" + n);
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).sendKeys("Troy");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//div[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck']/div/center")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).sendKeys("48084");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).sendKeys("MI");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//div[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id109']/div/table/tbody/tr[2]/td")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:Search")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2"))).selectByVisibleText("Other");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).sendKeys("Steady LLC");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft"))).selectByVisibleText("Limited Liability Company (LLC)");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).sendKeys("666" + n);
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).sendKeys("3130050045");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).sendKeys("3135574874");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).sendKeys("Rygst@tres.com");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).sendKeys("4400 corporate dr");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id379")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id379")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id379")).sendKeys("Troy");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id432")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id432")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id432")).sendKeys("48098");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//div[@id='pg:frm:pb:addresssection1:addresssection2']/div/table")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id625")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft"))).selectByVisibleText("Federal Employer Tax ID");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id625")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11"))).selectByVisibleText("Business Papers");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).clear();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).sendKeys("01/14/2009");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionRight:j_id95:docv")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).sendKeys("Steady");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1"))).selectByVisibleText("State");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1"))).selectByVisibleText("No");
        Thread.sleep(3000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1")).click();
        Thread.sleep(2000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:7:theitem:multiSelect | label=United States]]
        driver.findElement(By.xpath("(//option[@value='United States'])[2]")).click();
        Thread.sleep(2000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect | label=United States]]
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1"))).selectByVisibleText("2 - 5");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1"))).selectByVisibleText("$100,000 - $499,999");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1"))).selectByVisibleText("No");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1"))).selectByVisibleText("No");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1"))).selectByVisibleText("No");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2"))).selectByVisibleText("6-25");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:theitem:selectList4:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354"))).selectByVisibleText("PayUSA");
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id1086")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki"))).selectByVisibleText("1");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi"))).selectByVisibleText("1");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196"))).selectByVisibleText("Commercial Bank in the U.S.");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205"))).selectByVisibleText("Not Applicable");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196"))).selectByVisibleText("Corporation");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205"))).selectByVisibleText("Not Applicable");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id349:j_id351")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).sendKeys("rogers");
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).sendKeys("captain");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:Search")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).sendKeys("murphy");
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).clear();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).sendKeys("eddie");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:Applications:mainBlock:Search")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id315:j_id316:0:j_id318")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id349:j_id351")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:0:j_id223:0:j_id225 | label=Tax Reported Owner]]
        driver.findElement(By.xpath("//option[@value='Tax Reported Owner']")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:0:j_id223:1:j_id225 | label=Tax Reported Owner]]
        driver.findElement(By.xpath("(//option[@value='Tax Reported Owner'])[2]")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Authorized Signer]]
        driver.findElement(By.xpath("//option[@value='Authorized Signer']")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Authorized Signer]]
        driver.findElement(By.xpath("(//option[@value='Authorized Signer'])[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:ValidateSubmitEquifax")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Business Beneficial Owner]]
        driver.findElement(By.xpath("//option[@value='Business Beneficial Owner']")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Business Beneficial Owner]]
        driver.findElement(By.xpath("(//option[@value='Business Beneficial Owner'])[2]")).click();
        Thread.sleep(4000);
        // driver.findElement(By.id("j_id0:frm:equifaxTable:j_id369")).click();
        Thread.sleep(15000);
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Authorized Signer]]
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Business Beneficial Owner]]
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Not on Account]]
        driver.findElement(By.xpath("(//option[@value='Not on Account'])[2]")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Authorized Signer]]
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Business Beneficial Owner]]
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Not on Account]]
        driver.findElement(By.xpath("//option[@value='Not on Account']")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id369")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id218")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
        Thread.sleep(5000);
        driver.findElement(By.name("pg:frm:pb:j_id636")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("pg:frm:pb:j_id1016")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_local | ]]
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id369")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Authorized Signer]]
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:0:j_id225 | label=Not on Account]]
        driver.findElement(By.xpath("//option[@value='Authorized Signer']")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Authorized Signer]]
        // ERROR: Caught exception [ERROR: Unsupported command [removeSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Not on Account]]
        driver.findElement(By.xpath("(//option[@value='Authorized Signer'])[2]")).click();
        driver.findElement(By.id("bodyCell")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id369")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id267")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id267")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id267")).clear();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id267")).sendKeys("238150");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id269")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id269")).clear();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id269")).sendKeys("238150");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput")).click();
        new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput"))).selectByVisibleText("Pass");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput")).click();
        new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput"))).selectByVisibleText("Pass");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput")).clear();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput")).sendKeys("hjkhkj");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput")).click();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput")).clear();
        driver.findElement(By.id("j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput")).sendKeys("hjgj");
        Thread.sleep(1000);
        driver.findElement(By.id("bodyCell")).click();
        driver.findElement(By.name("j_id0:frm:equifaxTable:j_id447")).click();
        Thread.sleep(11000);
        driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id94")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id94"))).selectByVisibleText("Standard Business Checking");
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id94")).click();
        driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:j_id133:j_id138")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:j_id133:j_id138"))).selectByVisibleText("Yes");
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:j_id133:j_id138")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1"))).selectByVisibleText("General Operations");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1"))).selectByVisibleText("Yes - Cash and credit card");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:0")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:j_id723:childpanel:1:multiSelect3 | label=United States]]
        driver.findElement(By.xpath("//option[@value='United States']")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:j_id723:childpanel:0:j_id814:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1"))).selectByVisibleText("No but interested");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList3:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1"))).selectByVisibleText("In person - In Branch/ Office");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:multiSelect | label=Business Proceeds]]
        driver.findElement(By.xpath("//option[@value='Business Proceeds']")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:14:theitem:multiSelect | label=Salary]]
        driver.findElement(By.xpath("(//option[@value='Salary'])[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:15:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:b2cList:1:saving:svProd:j_id254")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:saving:svProd:j_id254"))).selectByVisibleText("Business Savings Plus Promo");
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:b2cList:1:saving:svProd:j_id254")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1"))).selectByVisibleText("Payroll");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1"))).selectByVisibleText("Yes - Check");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:4")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList3:0")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1"))).selectByVisibleText("No but interested");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1"))).selectByVisibleText("In person - In Branch/ Office");
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:14:theitem:multiSelect | label=Business Proceeds]]
        driver.findElement(By.xpath("(//option[@value='Business Proceeds'])[2]")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:15:theitem:multiSelect | label=Investments]]
        driver.findElement(By.xpath("(//option[@value='Investments'])[4]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:16:theitem:selectList3:1")).click();
        Thread.sleep(1000);
        driver.findElement(By.name("pg:frm:pb:j_id830")).click();
        driver.findElement(By.name("addser:frm:j_id985")).click();
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText")).click();
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Check");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText")).click();
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText")).click();
        new Select(driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Other");
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:j_id131:j_id133")).click();
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:j_id131:j_id133")).clear();
        driver.findElement(By.id("j_id0:frm:j_id83:j_id98:1:j_id100:j_id131:j_id133")).sendKeys("5500");
        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:frm:j_id83:j_id144:j_id148")).click();
        //acceptNextAlert = true;
        driver.findElement(By.id("j_id0:frm:j_id148:j_id206:setupUpAccountsbutton")).click();

    }


    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }

}
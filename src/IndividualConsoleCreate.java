import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class IndividualConsoleCreate {

    public static void main(String[] args) throws InterruptedException {
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Eclipse\\cHROMEdRIVER\\chromedriver.exe");

        WebDriver d = new ChromeDriver();

        String encodedPassword = "put encoded password here";

        d.get("https://test.salesforce.com");
        d.manage().window().maximize();
        Thread.sleep(3000);

        d.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncinodev");
        encrypt.setElement(d, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        d.findElement(By.xpath("//*[@id='Login']")).click();

        Thread.sleep(2000);

        // Run this section to ensure you're in the console
        d.findElement(By.xpath("//*[@id='BackToServiceDesk_Tab']/a")).click();
        d.findElement(By.xpath("//*[@id='00B16000009HIkq_listButtons']/ul/li[1]/input")).click();
        //*[@id="00B16000009HIkq_listButtons"]/ul/li[1]/input

    }

    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }
}

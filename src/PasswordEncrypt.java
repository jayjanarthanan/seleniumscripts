import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;

public class PasswordEncrypt {

    public static void main(String[] args) {

        String strMost = "vca184Frt#bakin";

        byte[] encode = Base64.encodeBase64(strMost.getBytes());

        System.out.println("String b4 encoding: " + strMost);

        System.out.println("String after encoding: " + new String(encode));

        byte[] decode = Base64.decodeBase64(encode);
        System.out.println("String after decoding: " + new String(decode));
    }

    public void setElement(WebDriver driver, final String locator, final String locatorValue, final String setValue) {
        WebDriverWait wait = new WebDriverWait(driver, 120);

        if (locator.equals("xpath")) {
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorValue)));
            element.clear();
            element.sendKeys(setValue);
        }
    }

    public PasswordEncrypt() {

    }

    public void closeAllBrowsers() {
        try {
            Runtime.getRuntime().exec("taskkill /IM geckodriver.exe /F");
            Runtime.getRuntime().exec("taskkill /IM chromedriver.exe /F");
            Runtime.getRuntime().exec("taskkill /IM IEDriverServer.exe /F");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createChromeDriver() throws Exception {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/" + "chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        //options.setExperimentalOption("excludeSwitches", Collections.singletonList("--enable-automation"));
        options.addArguments("--disable-infobars");
        options.addArguments("--enable-automation");
        DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome();
        handlSSLErr.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        //return new ChromeDriver(options);
        return;
    }
}
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class OneSavingsAcct {

    public static void main(String[] args) throws InterruptedException {

        //Login in sequence begin
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Eclipse\\cHROMEdRIVER\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        String encodedPassword = "put encoded password here";

        driver.get("https://test.salesforce.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncino");
        encrypt.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        driver.findElement(By.xpath("//*[@id='Login']")).click();
        //Login Sequence stop

        Thread.sleep(2000);


        try {
            driver.switchTo().frame("ext-comp-1015");
        } catch (Exception e) {
        }
        try {
            driver.switchTo().frame("ext-comp-1005");
        } catch (Exception e) {
        }
        try {
            driver.switchTo().frame("ext-comp-1011");
        } catch (Exception e) {
        }


        //Click into exisiting customer - first on the list
        driver.findElement(By.xpath("//*[@id='ext-gen12']/div[1]/table/tbody/tr/td[3]")).click();
        //*[@id="ext-gen12"]/div[1]/table/tbody/tr/td[3]
        driver.findElement(By.xpath("//*[@id='ext-gen12']/div[1]/table/tbody/tr/td[3]")).click();

        Thread.sleep(2000);

        //Click on detail view i.e get out of feed layout
        driver.findElement(By.xpath("//*[@id='efpViews_0012900000GNvZI_option1']/span[1]/span")).click();

        Thread.sleep(2000);

        //Click on 'new application'
        driver.findElement(By.xpath("//*[@id='massActionForm_0012900000GNvZI_00NG0000009bKMe']/div[1]/table/tbody/tr/td[2]/input")).click();

        //Step 2 - Need to verify everything is up to date
        //Click next
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:buttons']/input[2]")).click();
        Thread.sleep(5000);

        //Step 3 - Verify ID & KYC is up to date
        //Click Save & New Application
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id1019']")).click();
        Thread.sleep(5000);

        //Begin Account Opening
        //Click Continue button
        driver.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
        Thread.sleep(6000);

        //Selct 1 checking account
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki"))).selectByVisibleText("1");
        Thread.sleep(2000);

        Thread.sleep(2000);
        //Choose Checking Ownership Type
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196']/option[2]")).click();
        Thread.sleep(2000);
        //Choose Checking Ownership Subtype
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205']/option[2]")).click();
        Thread.sleep(2000);

        //Click Next Step button
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id349:bottom']/input[3]")).click();
        Thread.sleep(6000);
        //Select Primary Owner option for Checking
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[4]/td[2]/select/option[1]")).click();


        //Click Validate and Submit to Equifax button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:ValidateSubmitEquifax']")).click();
        Thread.sleep(5000);
        //Click Accept Credit Check Radio button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id406:0:j_id408:j_id410:0']")).click();
        Thread.sleep(1000);
        //Click Accept Debit Check Radio button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id412:0:j_id414:j_id416:0']")).click();
        Thread.sleep(1000);
        //Click Submit button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id418:equiFaxButton']")).click();
        Thread.sleep(3000);
        //Select the Pass option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput']/option[2]")).click();
        //Provide a comment
        Thread.sleep(4000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput']")).sendKeys("This is an automation Test Script");
        Thread.sleep(3000);
        // Select Pass for the Second option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput']/option[2]")).click();
        Thread.sleep(3000);
        // Provide a comment for 2nd option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput']")).sendKeys("The scrpit is still Running...Good Job!!");
        Thread.sleep(3000);
        //Click Next button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id446']/input")).click();
        Thread.sleep(7000);
        //Select a Checking Product Type
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:Product:j_id92']/div/select/option[2]")).click();
        Thread.sleep(2000);

        //Reg E Opt In Yes
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:j_id133:j_id136']/div/select/option[2]")).click();
        Thread.sleep(6000);

        //How many cash deposits or withdrawals do you expect to make per month, if any? 0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many electronic withdrawals or deposits from international companies/persons do you expect to conduct per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many domestic wire transfers do you anticipate sending or receiving per month, if any? 0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many international wire transfers will you send or receive per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //Do you intend to use your mobile phone to deposit checks remotely? No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1']")).click();
        Thread.sleep(2000);
        //How many domestic electronic withdrawals or deposits do you expect to conduct per month, if any? No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //What channel did the customer use to open the account?  In person - In Branch/Office
        new Select(driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1']"))).selectByValue("In person - In Branch/ Office");
        Thread.sleep(2000);
        // What is the source of initial funding for this account?
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:multiSelect']/option[4]")).click();
        Thread.sleep(3000);
        // What is the source of ongoing funding for this account?
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect']/option[3]")).click();
        Thread.sleep(2000);
        // SAVINGS  Is the customer opening a promotional 1.30% account today?  NO
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id232:0:questionOptions:1']")).click();
        Thread.sleep(4000);
        // Is the customer opening a promotional 1.15% account today?  No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id232:1:questionOptions:1']")).click();
        Thread.sleep(4000);

        //  Click Next On Account Setup Page
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id144:bottom']/input[3]")).click();
        Thread.sleep(5000);
        // Click "Setup Accounts" Button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id148:j_id206:setupUpAccountsbutton']")).click();
        Thread.sleep(3000);

        //Accept pop-up alert
        driver.switchTo().alert().accept();
        Thread.sleep(8000);

        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:0:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:1:j_id126:2']")).click();
        Thread.sleep(2000);
        //driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:2:j_id126:1']")).click();
        Thread.sleep(3000);

        // Click Finish button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:a:fin']")).click();
        Thread.sleep(5000);
        //driver.close();
    }

    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }

}

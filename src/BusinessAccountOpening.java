import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BusinessAccountOpening {

    public static void main(String[] args) throws InterruptedException {

        //Login sequence begin
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Dev\\IdeaProjects\\Selenium\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        //used to generate random 6 digit string for end of SSN
        //Random rnd = new Random();
        //int n = 100000 + rnd.nextInt(900000);

        //Set time stamp variable to use as first name
        //String timeStamp = new SimpleDateFormat("yyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        String encodedPassword = "put encoded password here";

        driver.get("https://test.salesforce.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncinotest");
        Thread.sleep(2000);
        encrypt.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));

        driver.findElement(By.xpath("//*[@id='Login']")).click();
        //Login sequence END

        //Begin find first existing Business Customer and create deposit account.
        //Select option 5: Business NonPerson TIN Last 30 Days list view and select first customer
        Thread.sleep(5000);

        driver.findElement(By.linkText("Customers")).click();
        Thread.sleep(2000);

        driver.findElement(By.id("fcf")).click();
        Thread.sleep(2000);

        new Select(driver.findElement(By.id("fcf"))).selectByVisibleText("05) Business NonPerson TIN Last 30 Days");
        Thread.sleep(2000);

        //dev select first row in list view
        try {
            driver.findElement(By.xpath("//*[@id='filter_element']/div/span/span[1]/input")).click();
            driver.findElement(By.xpath("//*[@id='ext-gen12']/div[1]/table/tbody/tr/td[3]")).click();
            driver.findElement(By.xpath("//*[@id=\'0012900000JtDZ1_ACCOUNT_NAME\']/a/span")).click();
            driver.findElement(By.xpath("//*[@id='001e0000018wzMF_ACCOUNT_NAME']")).click();
            driver.findElement(By.xpath("//*[@id='001e0000018wzMF_ACCOUNT_NAME']/a")).click();
            driver.findElement(By.id("0011h000008GNSx_ACCOUNT_NAME")).click();
            driver.findElement(By.xpath("//*[@id='0011h000008GNSx_ACCOUNT_NAME']/a")).click();
            driver.findElement(By.id("efpViews_001e0000018wzMF_option1")).click();
        } catch (Exception ignored) {
        }

        //Test
        try {
            driver.findElement(By.id("efpViews_0011h000008GNSx_option1")).click();

            driver.findElement(By.xpath("//*[@id='massActionForm_001e0000018wzMF_00NG0000009bKOc']/div[1]/table/tbody/tr/td[2]/input")).click();

        Thread.sleep(500);

            driver.findElement(By.name("new_application")).click();

        Thread.sleep(500);

            driver.findElement(By.name("pg:frm:pb:j_id625")).click();

            driver.findElement(By.name("pg:frm:pb:j_id599")).click();

        Thread.sleep(500);

            driver.findElement(By.name("pg:frm:pb:j_id607")).click();

            driver.findElement(By.name("pg:frm:pb:j_id1086")).click();
        } catch (Exception ignored) {
        }

        Thread.sleep(500);

        //Stop finding customer and begin deposit account opening
        //Update & Verification all done, Save and New application begin

        try {
            driver.findElement(By.name("save")).click();
        } catch (Exception ignored) {
        }

        driver.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();

        Thread.sleep(1000);
        //begin selecting accounts: 2x Checking and 1x Savings
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki"))).selectByVisibleText("2");
        Thread.sleep(1000);
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi"))).selectByVisibleText("1");
        Thread.sleep(2000);
        //Select types of accounts
        //checking 1
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196"))).selectByVisibleText("LLC");
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205"))).selectByVisibleText("Not Applicable");
        //Checking 2
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196"))).selectByVisibleText("LLC");
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205"))).selectByVisibleText("Not Applicable");
        //Savings 1
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:2:j_id196"))).selectByVisibleText("LLC");
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:2:j_id205"))).selectByVisibleText("Not Applicable");

        //add an existing customer, choose first in list
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).sendKeys("Test");
        driver.findElement(By.name("j_id0:Applications:mainBlock:Search")).click();
        Thread.sleep(7000);

        //dev select first individual in list
        try {
            driver.findElement(By.id("j_id0:Applications:mainBlock:j_id315:j_id316:0:j_id317")).click();
        } catch (Exception ignored) {
        }

        //Test select first individual in list
        try {
            driver.findElement(By.name("j_id0:Applications:mainBlock:j_id315:j_id316:0:j_id318")).click();
        } catch (Exception ignored) {
        }

        Thread.sleep(1000);
        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id315:j_id316:0:j_id318")).click();
        Thread.sleep(5000);
        //Finish adding existing customer and click 'next step'

        driver.findElement(By.name("j_id0:Applications:mainBlock:j_id349:j_id351")).click();

        //Begin Equifax Check

        driver.findElement(By.xpath("//option[@value='Tax Reported Owner']")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:0:j_id223:1:j_id225 | label=Tax Reported Owner]]
        driver.findElement(By.xpath("(//option[@value='Tax Reported Owner'])[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//option[@value='Tax Reported Owner'])[3]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//option[@value='Authorized Signer'])[3]")).click();
        Thread.sleep(1000);
        // ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id166:j_id209:1:j_id223:1:j_id225 | label=Authorized Signer]]
        driver.findElement(By.xpath("(//option[@value='Authorized Signer'])[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("(//option[@value='Authorized Signer'])[1]")).click();
        Thread.sleep(1000);


        //Begin update consumer added to business for Equifax check
        try {
            driver.findElement(By.id("j_id0:frm:equifaxTable:j_id167:j_id210:1:j_id219")).click();
            Thread.sleep(5000);

            String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                // Switch to child window
                driver.switchTo().window(winHandle);
            }

            //switch to child window of 1st child window.
            driver.findElement(By.name("pg:frm:pb:j_id636")).click();
            Thread.sleep(3000);
            driver.findElement(By.id("pg:frm:pb:j_id1016")).click();
            Thread.sleep(5000);
            driver.close();
            driver.switchTo().window(winHandleBefore);
        } catch (Exception ignored) {
        }

        //push button to submit to Equifax
        driver.findElement(By.id("j_id0:frm:equifaxTable:ValidateSubmitEquifax")).click();
        // ^ Submit and wait for 15 seconds v
        Thread.sleep(15000);

        //dev
        try {
            //Set pass / reasons
            new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id270:decisionTable:0:warningInput"))).selectByVisibleText("Pass");
            Thread.sleep(2000);
            new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id270:decisionTable:1:warningInput"))).selectByVisibleText("Pass");
            Thread.sleep(2000);
            driver.findElement(By.id("j_id0:frm:equifaxTable:j_id270:decisionTable:0:warningOverrideInput")).sendKeys("Test Script says this is okay");
            Thread.sleep(1000);
            driver.findElement(By.id("j_id0:frm:equifaxTable:j_id270:decisionTable:1:warningOverrideInput")).sendKeys("So this is okay");
            Thread.sleep(1000);
        } catch (Exception ignored) {
        }

        //Test
        try {
            new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id272:decisionTable:0:warningInput"))).selectByVisibleText("Pass");
            Thread.sleep(2000);
            new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id272:decisionTable:1:warningInput"))).selectByVisibleText("Pass");
            Thread.sleep(2000);
            driver.findElement(By.id("j_id0:frm:equifaxTable:j_id272:decisionTable:0:warningOverrideInput")).sendKeys("Test Script says this is okay");
            Thread.sleep(1000);
            driver.findElement(By.id("j_id0:frm:equifaxTable:j_id272:decisionTable:1:warningOverrideInput")).sendKeys("So this is okay");
            Thread.sleep(1000);
        } catch (Exception ignored) {
        }

        //Next button
        driver.findElement(By.name("j_id0:frm:equifaxTable:j_id448")).click();

        Thread.sleep(5000);

        //Finish Equifax Check

        //begin step 3 (Account Details) - No promos in this one

        //Checking 1 begin
        //Choose product
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id96"))).selectByVisibleText("Standard Business Checking");
        Thread.sleep(3000);

        //Begin the list of questions
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:0:j_id672"))).selectByVisibleText("Payroll");
        Thread.sleep(2000);
        //Select no for beneficial ownership
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:2:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:4:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:6:j_id672"))).selectByVisibleText("Yes - Cash");
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:7:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:11:j_id678:4")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:12:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:13:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:15:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:19:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:21:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:25:j_id672"))).selectByVisibleText("Yes");
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:26:j_id678:1")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:28:j_id672"))).selectByVisibleText("In person - In Branch/ Office");
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id599']/table/tbody/tr[31]/td[3]/select/option[1]")).click();
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id599']/table/tbody/tr[33]/td[3]/select/option[1]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id599:j_id600:j_id662:34:j_id678:1")).click();
        //Checking 1 END

        Thread.sleep(5000);

        //Checking 2 begin
        //Choose product
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:Checking:Product:j_id96"))).selectByVisibleText("SimplyBusiness Checking - FOR COMMERCIAL CUSTOMERS ONLY");
        Thread.sleep(3000);

        //Begin list of questions for 2nd product
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:0:j_id672"))).selectByVisibleText("Payroll");
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:2:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:4:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:6:j_id672"))).selectByVisibleText("Yes - Cash");
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:7:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:11:j_id678:4")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:12:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:13:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:15:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:19:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:21:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:25:j_id672"))).selectByVisibleText("Yes");
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:26:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:26:j_id678:1")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:28:j_id672"))).selectByVisibleText("In person - In Branch/ Office");
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id599']/table/tbody/tr[31]/td[3]/select/option[1]")).click();
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id599']/table/tbody/tr[33]/td[3]/select/option[1]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:1:kycSection2:j_id599:j_id600:j_id662:34:j_id678:1")).click();
        //Checking 2 END

        Thread.sleep(3000);

        //Savings Begin
        //Choose product
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:2:saving:svProd:j_id259"))).selectByVisibleText("Business Savings Plus");
        Thread.sleep(3000);

        /* Begin list of questions for 3rd product */
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:0:j_id672"))).selectByVisibleText("Payroll");
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:2:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:4:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:6:j_id672"))).selectByVisibleText("Yes - Cash");
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:7:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:11:j_id678:4")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:12:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:13:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:15:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:19:j_id678:0")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:21:j_id678:0")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:25:j_id672"))).selectByVisibleText("Yes");
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:26:j_id678:1")).click();
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:26:j_id678:1")).click();
        new Select(driver.findElement(By.name("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:28:j_id672"))).selectByVisibleText("In person - In Branch/ Office");
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:kycSection2:j_id599']/table/tbody/tr[31]/td[3]/select/option[1]")).click();
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:kycSection2:j_id599']/table/tbody/tr[33]/td[3]/select/option[1]")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("pg:frm:pb:b2cList:2:kycSection2:j_id599:j_id600:j_id662:34:j_id678:1")).click();

        //click Next button after answering all the questions
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id699']/a")).click();

        //click past screen 4, select nothing
        driver.findElement(By.xpath("//*[@id='addser:frm']/center/input[3]")).click();

        //Screen 5
        new Select(driver.findElement(By.name("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Cash");
        Thread.sleep(1000);
        new Select(driver.findElement(By.name("j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Cash");
        Thread.sleep(1000);
        new Select(driver.findElement(By.name("j_id0:frm:j_id83:j_id98:2:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Cash");
        Thread.sleep(1000);

        driver.findElement(By.name("j_id0:frm:j_id83:j_id144:j_id148")).click();

        //Screen 6
        //click 'Set up Account(s)'
        driver.findElement(By.id("j_id0:frm:j_id148:j_id206:setupUpAccountsbutton")).click();

        //select JS pop-up
        driver.switchTo().alert().accept();
        Thread.sleep(5000);

        //Screen 7
        // Not interested
        try {
            driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:0:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:1:j_id126:2']")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:2:j_id126:1']")).click();
            Thread.sleep(2000);
            //Put a Comment
            driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id201:j_id202:textAreaDelegate_CrossSell_Notes__c_rta_body']")).sendKeys("This AUTOMATION test is almost done");
            Thread.sleep(8000);
        } catch (Exception ignored) {
        }

        // Click Finish button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:a:fin']")).click();
        Thread.sleep(8000);

    }

    private static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }
}

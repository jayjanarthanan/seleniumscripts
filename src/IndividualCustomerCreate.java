import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IndividualCustomerCreate {

    public static void main(String[] args) throws InterruptedException {
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Eclipse\\cHROMEdRIVER\\chromedriver.exe");

        WebDriver d = new ChromeDriver();

        String encodedPassword = "put encoded password here";

        d.get("https://test.salesforce.com");
        d.manage().window().maximize();
        Thread.sleep(2000);

        d.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncinodev");
        encrypt.setElement(d, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        d.findElement(By.xpath("//*[@id='Login']")).click();

        Thread.sleep(2000);

        //This section selects the option to create a new customer and select the Individual / person account record type
        d.findElement(By.xpath("//*[@id='Account_Tab']/a")).click();
        d.findElement(By.xpath("//*[@id='hotlist']/table/tbody/tr/td[2]/input")).click();
        new Select(d.findElement(By.xpath("//*[@id='p3']"))).selectByVisibleText("FSBIndividual");

        d.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
        Thread.sleep(3000);
        //This section is for customer search
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[1]/input")).sendKeys("FSBBusiness");
        //*[@id="j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80"]/div[2]/table/tbody/tr[1]/td[1]/input
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("AutoOne1");
        //street
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[3]/td[1]/input")).sendKeys("2575 Somerset Blvd");
        //Tax ID
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("666322018");
        //City
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103']")).sendKeys("Troy");
        //State
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103']")).sendKeys("Michigan");
        //Zip
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103']")).sendKeys("48084");
        //Click Search
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:Search']")).click();
        //Click Create New customer
        Thread.sleep(5000);
        d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew']")).click();

        //How did you hear about Flagstar
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2']/option[7]")).click();
        //Phone
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11']")).sendKeys("9147857874");
        Thread.sleep(2000);

        //Confirm Tax ID
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4']")).sendKeys("666322018");

        //Email
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9']")).sendKeys("AutoTest@test.com");

        //Mothers Maiden Name
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).sendKeys("Selenium");
        Thread.sleep(6000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).sendKeys("Selenium");

        //DOB
        Thread.sleep(5000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9']")).sendKeys("10/29/1975");

        //Street
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section15:j_id367']")).sendKeys("2785 Golfview Drive");

        //City
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section16:j_id374']")).sendKeys("Troy");
        //Zip
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section20:j_id427']")).sendKeys("48084");
        //State
        Thread.sleep(2000);
        new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section17:j_id400']"))).selectByVisibleText("Michigan");
        //Click Next
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:buttons']/input[2]")).click();
        Thread.sleep(4000);
        // ID Document type
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:item3:iddt11']/option[2]")).click();
        //DL #
        Thread.sleep(5000);

        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:j_id103:iddn']")).sendKeys("P0784572501541");

        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:j_id171:expd']")).sendKeys("10/25/2022");

        //Secondary ID  Select Credit Card
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id203:type2']/option[3]")).click();
        //Card Type Select Visa
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id209:ctype']/option[5]")).click();
        //Last Four digits of card
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id214:aiddn']")).sendKeys("6465");
        //Exp. Date
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id287:aexpd']")).sendKeys("10/25/2022");
        Thread.sleep(2000);
        //Employment Status Select Employed
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1']/option[2]")).click();
        //Type of occupation Select business Services
        Thread.sleep(2000);

        try {

            new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:0:j_id517']/div/select"))).selectByVisibleText("Automotive Sales and Repair Services");

        } catch (Exception e) {

        }
        try {

            new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:1:j_id517']/div/select"))).selectByVisibleText("Automotive Sales and Repair Services");

        } catch (Exception e) {

        }

        //What is your position
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:2:j_id450']/div/input")).sendKeys("Racecar driver");

        //Who is your employer
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:3:j_id450']/div/input")).sendKeys("Nascar");

        // How long employed
        Thread.sleep(2000);
        new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:5:j_id522']/select"))).selectByVisibleText("3+");

        //Select Full-Time Radio button
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:6:j_id532:0']")).click();
        Thread.sleep(4000);

        //Are you a Citizen
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1']/option[2]")).click();

        Thread.sleep(3000);

        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect']/option[1]")).click();
        Thread.sleep(2000);
        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:3:theitem:multiSelect']/option[1]")).click();
        Thread.sleep(2000);

        d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id1014']")).click();
        //done
    }

    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }
}

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import scala.sys.SystemProperties;

import java.util.Random;

class IndvCreateAndDepositAcctOpening {

    public static void main(String[] args) throws InterruptedException {

        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Dev\\IdeaProjects\\Selenium\\chromedriver.exe");

        WebDriver d = new ChromeDriver();

        //used to generate random 6 digit string for end of SSN
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);

        //Set time stamp variable to use as first name
        //String timeStamp = new SimpleDateFormat("HH_mm_ss").format(new java.util.Date());

        String encodedPassword = "dmNhMTg0RnJ0I2Jha2lu";

        d.get("https://test.salesforce.com");
        d.manage().window().maximize();
        Thread.sleep(5000);

        d.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.test");
        encrypt.setElement(d, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        d.findElement(By.xpath("//*[@id='Login']")).click();
        //d.findElement(xpa)
        Thread.sleep(2000);
    /*
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_SPACE);
        robot.keyPress(KeyEvent.VK_N);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_SPACE);
        robot.keyRelease(KeyEvent.VK_N);
    */

        try {


            //This section selects the option to create a new customer and select the Individual / person account record type
            d.findElement(By.xpath("//*[@id='Account_Tab']/a")).click();
            d.findElement(By.xpath("//*[@id='hotlist']/table/tbody/tr/td[2]/input")).click();
            new Select(d.findElement(By.xpath("//*[@id='p3']"))).selectByVisibleText("Consumer Customer");

            Thread.sleep(4000);

            d.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();

            Thread.sleep(4000);

            //This section is for customer search

            //first name
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[1]/input")).sendKeys("Smith");
            Thread.sleep(2000);
            //Last name
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("Selenium");
            //street
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[3]/td[1]/input")).sendKeys("2575 Somerset Blvd");

            System.out.println("got to here");

            //Tax ID
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("666" + n);
            //City
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103']")).sendKeys("Troy");
            //State
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103']")).sendKeys("Michigan");
            //Zip
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103']")).sendKeys("48084");
            //Click Search
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:Search']")).click();
            //Click Create New customer
            Thread.sleep(5000);
            d.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew']")).click();

            System.out.println("got to here2");


            //Phone
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11']")).sendKeys("9147857874");
            Thread.sleep(2000);

            //Confirm Tax ID
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4']")).sendKeys("666" + n);

            //Email
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id=\'pg:frm:pb:PgSection121:j_id347:section6:5:section7:section12\']")).click();

            //Mothers Maiden Name
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).sendKeys("Selenium");
            Thread.sleep(6000);

            //DOB
            Thread.sleep(5000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9']")).sendKeys("10/29/1975");

            //Street
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section15:j_id367']")).sendKeys("2785 Golfview Drive");

            //City
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section16:j_id374']")).sendKeys("Troy");
            //Zip
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section20:j_id427']")).sendKeys("48084");
            //State
            Thread.sleep(2000);
            new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section17:j_id400']"))).selectByVisibleText("Michigan");

            System.out.println("got to here3");

            //Click Next
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:buttons']/input[2]")).click();

            Thread.sleep(7000);

            // ID Document type
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:item3:iddt11']/option[2]")).click();
            //DL #
            Thread.sleep(5000);

            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:j_id106:iddn']")).sendKeys("P0784572501541");

            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:j_id174:expd']")).sendKeys("10/25/2022");

            //Secondary ID  Select Credit Card
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id206:type2']/option[3]")).click();

            //Card Type Select Visa
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id215:ctype']/option[5]")).click();

            //Last Four digits of card
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id222:aiddn']")).sendKeys("6465");

            //Exp. Date
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id296:aexpd']")).sendKeys("10/25/2022");
            Thread.sleep(2000);


            //Employment Status Select Employed
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1']")).click();
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1']/option[2]")).click();
            Thread.sleep(2000);

            new Select(d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:0:j_id527']/div/select"))).selectByVisibleText("Automotive Sales and Repair Services");

            //What is your position
            Thread.sleep(4000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:2:j_id460']/div/input")).sendKeys("Racecar driver");

            //Who is your employer
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:3:j_id460']/div/input")).sendKeys("Nascar");

            Thread.sleep(4000);

            //Are you a Citizen
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1']/option[2]")).click();

            Thread.sleep(3000);

            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect']/option[1]")).click();
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:3:theitem:multiSelect']/option[1]")).click();
            Thread.sleep(2000);

            //Click Save & New Application button
            Thread.sleep(3000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id1029']")).click();
            Thread.sleep(8000);

    //End Customer Create

    //Open Deposit Account
            //Click Continue button
            d.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
            Thread.sleep(6000);

            //Selct 1 checking account
            new Select(d.findElement(By.id("j_id0:Applications:mainBlock:j_id159:j_id164:checki"))).selectByVisibleText("1");
            Thread.sleep(2000);

            //Choose Checking Ownership Type
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id195:0:j_id197']/option[2]")).click();
            Thread.sleep(2000);

            //Choose Checking Ownership Subtype
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id202:0:j_id206']/option[2]")).click();
            Thread.sleep(2000);

            //Selct 1 savings account
            new Select(d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id159:j_id173:savingsi']"))).selectByValue("1");
            Thread.sleep(2000);

            //Choose Savings Ownership type
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id195:1:j_id197']/option[2]")).click();
            Thread.sleep(1000);

            //Choose Savings Ownership Subtype
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id202:1:j_id206']/option[2]")).click();

            //select 1 CD
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id159:j_id179:td']/option[2]")).click();
            Thread.sleep(2000);

            //Choose CD Ownership
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id195:2:j_id197']/option[2]")).click();
            Thread.sleep(2000);

            //Choose CD subtype
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id187:j_id202:2:j_id206']/option[2]")).click();
            Thread.sleep(2000);

            //Click Next Step button
            d.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id490:bottom']/input[3]")).click();
            Thread.sleep(6000);




            //Select Primary Owner option for Checking
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id167:equifaxpanel']/div/center/table/tbody/tr[4]/td[2]/select/option[1]")).click();

            //Select Primary Owner option for Savings
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id167:equifaxpanel']/div/center/table/tbody/tr[4]/td[3]/select/option[1]")).click();

            //Select Primary Owner option for CD
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id167:equifaxpanel']/div/center/table/tbody/tr[4]/td[4]/select/option[1]")).click();

            //Click Validate and Submit to Equifax button
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:ValidateSubmitEquifax']")).click();
            Thread.sleep(5000);

            //Click Accept Credit Check Radio button
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id449:j_id455:0:j_id457:j_id459:0']")).click();
            Thread.sleep(1000);

            //Click Accept Debit Check Radio button
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id449:j_id461:0:j_id463:j_id465:0']")).click();
            Thread.sleep(1000);

            //Click Submit button
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id449:j_id467:equiFaxButton']")).click();
            Thread.sleep(15000);


            //Select the Pass option for 1
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningInput']/option[2]")).click();

            //Provide a comment
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput']")).clear();
            Thread.sleep(1000);
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput']")).click();
            Thread.sleep(1000);
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput']")).sendKeys("This is an automation Test Script");
            Thread.sleep(1000);


            //Click Next button
            d.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id495']/input")).click();

            Thread.sleep(7000);

            //Select a Checking Product Type
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:Product:j_id96']/div/select/optgroup/option[1]")).click();
                Thread.sleep(5000);

                //Reg E Opt In Yes
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:j_id143:regOption']/option[2]")).click();

                Thread.sleep(5000);

                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody")).click();

                //Begin KYC
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:0:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:2:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:6:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:8:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:12:j_id706:1']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:14:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody/tr[17]/td[3]/select/option[2]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody/tr[19]/td[3]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody/tr[19]/td[3]/select/option[2]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody/tr[21]/td[3]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection2:j_id627']/table/tbody/tr[21]/td[3]/select/option[1]")).click();
                //End KYC

            //Savings start
                //No Promo
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id247:0:questionOptions:1']")).click();
                Thread.sleep(5000);
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id247:2:questionOptions:1']")).click();
                Thread.sleep(6000);

                //Account Type = Simply Savings
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:svProd:j_id270']/div/select/optgroup/option[2]")).click();

                Thread.sleep(6000);

                //Begin KYC
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:0:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:2:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:6:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:8:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:12:j_id706:1']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627:j_id628:j_id690:14:j_id706:0']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627']/table/tbody/tr[17]/td[3]/select/option[2]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627']")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627']/table/tbody/tr[19]/td[3]/select/option[2]")).click();
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection2:j_id627']/table/tbody/tr[21]/td[3]/select/option[1]")).click();
                //END KYC

            //CD Start
                //NO PROMO
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:TDSection:j_id392:0:questionOptions:1']")).click();
                Thread.sleep(3000);

                //30-89 day
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:TDSection:j_id413:j_id416']/div/select/optgroup/option[3]")).click();
                Thread.sleep(7000);

                //Override Rate
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:TDSection:j_id431:j_id442']")).click();
                Thread.sleep(5000);
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:TDSection:j_id448:j_id450']/div/input")).sendKeys("No big deal, give em big rates");
                d.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:2:TDSection:j_id468:j_id472']")).sendKeys("100");
                Thread.sleep(2000);

            //Click Next
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id727']/a")).click();
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='pg:frm:pb:j_id727']/a")).click();

            /*
                //Add Debit Card
                d.findElement(By.xpath("//*[@id='addser:frm:pgblck:pgblcksection:DebitCard:j_id103']")).click();
                Thread.sleep(5000);

                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:serviceName:j_id476:j_id478']/div/select/option[2]")).click();
                                        //*[@id="addser:frm:frm2:j_id377:0:serviceName:j_id476:j_id478"]/div/select/option[2]
                Thread.sleep(3000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:debitsec:j_id504:cardType']")).click();
                Thread.sleep(1000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:debitsec:j_id504:cardType']")).clear();
                Thread.sleep(1000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:debitsec:j_id504:cardType']")).sendKeys("Flagstar Visa Debit Card");
                Thread.sleep(2000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:debitsec:j_id504:cardType']")).sendKeys("Flagstar Visa Debit Card");
                Thread.sleep(1000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:debitsec:j_id504:cardType']")).click();
                Thread.sleep(1000);


                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:j_id546:j_id564:0:j_id566']/input")).click();
                Thread.sleep(2000);
                d.findElement(By.xpath("//*[@id='addser:frm:frm2:j_id377:0:checkingSec']/div/table/tbody/tr[1]/td[1]/select/option[2]")).click();
                Thread.sleep(3000);
                //Done w/ Debit Card
            */


            //Next
            d.findElement(By.xpath("//*[@id='addser:frm']/center/input[3]")).click();

            //Choose funding source for Checking
            d.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText']/option[2]")).click();

            d.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:0:j_id140']/input")).click();
            Thread.sleep(3000);

            //set funding amount for CD
            d.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:2:j_id100:j_id127:j_id132']")).sendKeys("5000");

            //Next
            d.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id147:bottom']/input[3]")).click();

            Thread.sleep(5000);

            //Set up accounts
            d.findElement(By.xpath("//*[@id='j_id0:frm:j_id195:j_id253:setupUpAccountsbutton']")).click();
            Thread.sleep(5000);

            try{
                /* Accepting alert. */
                Alert alert;
                alert = d.switchTo().alert();

                alert.accept();

                System.out.println("Accepted the alert successfully.");

            }catch(Throwable e){
                System.err.println("Error came while waiting for the alert popup. "+e.getMessage());
            }

            Thread.sleep(15000);

            //cross sell screen
            d.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id102:0:pgblcksection:j_id105:0:j_id106:0:pbs1:j_id107:0:j_id125:2']")).click();
            Thread.sleep(2000);
            d.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id102:0:pgblcksection:j_id105:1:j_id106:0:pbs1:j_id107:0:j_id125:2']")).click();
            Thread.sleep(2000);

            //Finish
            d.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:a:fin']")).click();

            Thread.sleep(7000);

            //Generate Docs
            try{
                d.switchTo().frame("066G0000001gkh7");
            }catch (Exception e){}

            d.findElement(By.xpath("//*[@id='j_id0:frm:displayBlock']/div[1]")).click();
            System.out.println("clicked into table");
            d.findElement(By.xpath("//*[@id='j_id0:frm:displayBlock:j_id124:0:genDocBtn']")).click();
            //*[@id="j_id0:frm:displayBlock:j_id124:0:genDocBtn"]
            Thread.sleep(1000);
            d.findElement(By.xpath("//*[@id='j_id0:frm:displayBlock:j_id124:1:genDocBtn']")).click();
            Thread.sleep(1000);
            d.findElement(By.xpath("//*[@id='j_id0:frm:displayBlock:j_id124:2:genDocBtn']")).click();


            System.out.println("Script completed, awesome!");

            /*
            try {

                String winHandleBefore = driver.getWindowHandle();
                for (String winHandle : driver.getWindowHandles())
                {
                    // Switch to child window
                    driver.switchTo().window(winHandle);
                }
                //switch to child window of 1st child window.

                driver.findElement(By.xpath()


                Thread.sleep(3000);
                driver.findElement(By.id("Do some stuff")).click();
                Thread.sleep(5000);

                driver.close();
                driver.switchTo().window(winHandleBefore);
                // Click Next button on the Update Customer page


            } catch (Exception e) {

                e.printStackTrace();
            }
           */


        } catch (Exception e) {
            encrypt.closeAllBrowsers();
        }
    }

    private static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }

}

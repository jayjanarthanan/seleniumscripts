import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateSingleBusiness {

    public static void main(String[] args) throws InterruptedException {

        //Login in sequence begin
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Dev\\IdeaProjects\\Selenium\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        String encodedPassword = "dmNhMTg0RnJ0I2Jha2k=";

        driver.get("https://test.salesforce.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncinotest");
        encrypt.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        driver.findElement(By.xpath("//*[@id='Login']")).click();
        //Login Sequence stop

        Thread.sleep(5000);

        driver.get("https://flagstar--ncino.cs70.my.salesforce.com/001/o");
        driver.findElement(By.name("new")).click();
        driver.findElement(By.id("ep")).click();
        driver.findElement(By.id("p3")).click();
        new Select(driver.findElement(By.id("p3"))).selectByVisibleText("FSBBusiness");
        driver.findElement(By.id("p3")).click();
        driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
        driver.findElement(By.xpath("//div[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[2]/td")).click();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).click();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).clear();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).sendKeys("Carwash");
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).clear();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).sendKeys("Pros");
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id87:j_id89")).clear();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id87:j_id89")).sendKeys("Mega Carwash");
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).clear();
        driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).sendKeys("666559999");
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:Search")).click();
        driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft"))).selectByVisibleText("Corporation");
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft"))).selectByVisibleText("Federal Employer Tax ID");
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).clear();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).sendKeys("666559999");
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).clear();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).sendKeys("2489414642");
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).click();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).clear();
        driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).sendKeys("prosenjit.paul@flagstar.com");
        driver.findElement(By.xpath("//div[@id='pg:frm:pb']/div")).click();
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).click();
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).clear();
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id372")).sendKeys("255 Main St");
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id379")).clear();
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id379")).sendKeys("Troy");
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id432")).clear();
        driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id432")).sendKeys("48098");
        driver.findElement(By.xpath("//div[@id='pg:frm:pb:addresssection1:addresssection7']/div/table/tbody/tr[3]/th")).click();
        driver.findElement(By.name("pg:frm:pb:j_id625")).click();
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
        new Select(driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11"))).selectByVisibleText("Articles of Incorporation");
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
        driver.findElement(By.linkText("3/6/2018")).click();
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).clear();
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).sendKeys("3/6/2018");
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).clear();
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).sendKeys("3/6/2000");
        driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionRight:j_id95:docv")).click();
        driver.findElement(By.name("pg:frm:pb:j_id1081")).click();
    }

    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }

}

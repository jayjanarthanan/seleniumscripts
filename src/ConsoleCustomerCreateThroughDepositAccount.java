import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ConsoleCustomerCreateThroughDepositAccount {

    public static void main(String[] args) throws InterruptedException {
        //Login in sequence begin
        PasswordEncrypt encrypt = new PasswordEncrypt();
        System.setProperty("webdriver.chrome.driver", "C:\\Eclipse\\cHROMEdRIVER\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        String encodedPassword = "put encoded password here";


        //driver.get("https://flagstar--ncino.cs70.my.salesforce.com/");
        driver.get("https://test.salesforce.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id='username']")).sendKeys("luigi.franciosi@flagstar.com.ncinodev");
        encrypt.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));
        driver.findElement(By.xpath("//*[@id='Login']")).click();
        //Login in sequence ends

        Thread.sleep(3000);


        try {
            driver.switchTo().frame("ext-comp-1015");
        } catch (Exception e) {
        } finally {
            driver.switchTo().frame("ext-comp-1011");
        }


        Thread.sleep(3000);

        // Select FSBBusiness
        driver.findElement(By.xpath("//*[@id='p3']/option[6]")).click();

        // Click Continue Button after Selecting FSBIndividual
        //driver.findElement(By.xpath("/html/body/form/div/div[3]/table/tbody/tr/td[2]/input[1]")).click();
        driver.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
        Thread.sleep(3000);

        try {
            driver.switchTo().frame("ext-comp-1038");
        } catch (Exception e) {
        } finally {
            driver.switchTo().frame("ext-comp-1034");
        }


        // Enter Last Name
        Thread.sleep(3000);

        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[1]/input")).sendKeys("Willit");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("Run");
        //street
        Thread.sleep(2000);
        //driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[3]/td[1]/input")).sendKeys("2575 Somerset Blvd");
        //Tax ID
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("666065656");
        //City
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103']")).sendKeys("Troy");
        //State
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103']")).sendKeys("Michigan");
        //Zip
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103']")).sendKeys("48084");
        //Click Search
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:Search']")).click();
        //Click Create New customer
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew']")).click();
        //How did you hear about Flagstar
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2']/option[7]")).click();
        //Phone
        Thread.sleep(2000);

        //driver.switchTo().frame("ext-comp-1034");
        Thread.sleep(3000);
        //driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4']")).sendKeys("7184587854");
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11']")).sendKeys("9147857870");
        Thread.sleep(2000);
        //Tax ID
        //driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:5:section2:section4']")).sendKeys("888118322");
        //Confirm Tax ID
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4']")).sendKeys("666065656");
        //Email
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9']")).sendKeys("Flagstar1@test.com");
        //Mothers Maiden Name
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).sendKeys("Selenium");
        //DOB
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9']")).sendKeys("10/29/1977");
        //Street
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section15:j_id367']")).sendKeys("1587 Golfview Drive");
        //City
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section16:j_id374']")).sendKeys("Troy");
        //Zip
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:addresssection1:addresssection2:section20:j_id427']")).sendKeys("48084");
        //Click Next
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:buttons']/input[2]")).click();
        Thread.sleep(4000);
        // ID Document type
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:item3:iddt11']/option[2]")).click();
        //DL #
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection:j_id103:iddn']")).sendKeys("P0784572501533");
        Thread.sleep(2000);
        //ID exp. date
        driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id171:expd")).sendKeys("09/28/2021");
        //Secondary ID  Select Credit Card
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id203:type2']/option[3]")).click();
        //Card Type Select Visa
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id209:ctype']/option[5]")).click();
        //Last Four digits of card
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id214:aiddn']")).sendKeys("6465");
        //Exp. Date
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:pgSection1:j_id287:aexpd']")).sendKeys("10/15/2021");
        Thread.sleep(2000);
        //Employment Status Select Employed
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1']/option[2]")).click();
        //Type of occupation Select business Services
        Thread.sleep(2000);
        //new Select (driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:0:j_id517']/div/select"))).
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:1:j_id517']/div/select/option[3]")).click();
        //What is your position
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:2:j_id450']/div/input")).sendKeys("Racecar driver");
        //Who is your employer
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:3:j_id450']/div/input")).sendKeys("Nascar");
        // How long employed
        Thread.sleep(2000);
        new Select(driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:5:j_id522']/select"))).selectByVisibleText("3+");
        //Select Full-Time Radio button
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:0:j_id437:childpanel:6:j_id532:0']")).click();
        //Are you a Citizen
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1']/option[2]")).click();
        Thread.sleep(2000);
        //Do you have income from any other source
        new Select(driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect']"))).selectByValue("Rental Income");
        //driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect']/option[1]")).click();
        Thread.sleep(2000);
        //What is the source of your assets or balances
        new Select(driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:3:theitem:multiSelect']"))).selectByValue("Personal Savings");
        //driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id304:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect']/option[9]")).click();
        //Click Save & New Application button
        Thread.sleep(3000);
        driver.findElement(By.id("pg:frm:pb:j_id1019")).click();
        Thread.sleep(6000);
        //Deposit Account


        //nCino Sandbox           //*[@id="p3"]/option[1]
        //driver.findElement(By.xpath("//*[@id='p3']/option[1]")).click();

        // nCinco3 sandbox      //*[@id="bottomButtonRow"]/input[1]
        //driver.findElement(By.xpath("//*[@id='p3']/option[2]")).click();
        //new Select(driver.findElement(By.xpath("//*[@id='p3']"))).selectByVisibleText("Deposit Account");

        //Deposit Account
        //Click Continue button
        driver.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
        Thread.sleep(6000);
        //Selct 1 checking account
        new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki"))).selectByVisibleText("1");
        Thread.sleep(2000);
        //Selct 1 savings account
        new Select(driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id158:j_id172:savingsi']"))).selectByValue("1");
        Thread.sleep(2000);
        //Choose Checking Ownership Type
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196']/option[2]")).click();
        Thread.sleep(2000);
        //Choose Checking Ownership Subtype
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205']/option[2]")).click();
        Thread.sleep(2000);
        //Choose Savings Ownership type
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196']/option[2]")).click();
        Thread.sleep(1000);
        //Choose Savings Ownership Subtype
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205']/option[2]")).click();
        //Click Next Step button
        driver.findElement(By.xpath("//*[@id='j_id0:Applications:mainBlock:j_id349:bottom']/input[3]")).click();
        Thread.sleep(6000);
        //Select Primary Owner option for Checking
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[4]/td[2]/select/option[1]")).click();
        //Select Primary Owner option for Savings
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[4]/td[3]/select/option[1]")).click();
        //Click Validate and Submit to Equifax button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:ValidateSubmitEquifax']")).click();
        Thread.sleep(5000);
        //Click Accept Credit Check Radio button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id406:0:j_id408:j_id410:0']")).click();
        Thread.sleep(1000);
        //Click Accept Debit Check Radio button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id412:0:j_id414:j_id416:0']")).click();
        Thread.sleep(1000);
        //Click Submit button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id400:j_id418:equiFaxButton']")).click();
        Thread.sleep(3000);
        //Select the Pass option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput']/option[2]")).click();
        //Provide a comment
        Thread.sleep(4000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput']")).sendKeys("This is an automation Test Script");
        Thread.sleep(3000);
        // Select Pass for the Second option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput']/option[2]")).click();
        Thread.sleep(3000);
        // Provide a comment for 2nd option
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput']")).sendKeys("The scrpit is still Running...Good Job!!");
        Thread.sleep(3000);
        //Click Next button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id446']/input")).click();
        Thread.sleep(7000);
        //Select a Checking Product Type
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:Product:j_id92']/div/select/option[2]")).click();
        Thread.sleep(2000);

        //Reg E Opt In Yes
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:j_id133:j_id136']/div/select/option[2]")).click();
        Thread.sleep(6000);

        //How many cash deposits or withdrawals do you expect to make per month, if any? 0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many electronic withdrawals or deposits from international companies/persons do you expect to conduct per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many domestic wire transfers do you anticipate sending or receiving per month, if any? 0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //How many international wire transfers will you send or receive per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //Do you intend to use your mobile phone to deposit checks remotely? No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1']")).click();
        Thread.sleep(2000);
        //How many domestic electronic withdrawals or deposits do you expect to conduct per month, if any? No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        //What channel did the customer use to open the account?  In person - In Branch/Office
        new Select(driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1']"))).selectByValue("In person - In Branch/ Office");
        Thread.sleep(2000);
        // What is the source of initial funding for this account?
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:multiSelect']/option[4]")).click();
        Thread.sleep(3000);
        // What is the source of ongoing funding for this account?
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect']/option[3]")).click();
        Thread.sleep(2000);
        // SAVINGS  Is the customer opening a promotional 1.30% account today?  NO
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id232:0:questionOptions:1']")).click();
        Thread.sleep(4000);
        // Is the customer opening a promotional 1.15% account today?  No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:j_id232:1:questionOptions:1']")).click();
        Thread.sleep(4000);
        // Product  ...........Simply Savings
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:svProd:j_id252']/div/select/option[2]")).click();
        Thread.sleep(2000);
        // How many cash deposits or withdrawals do you expect to make per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        // How many electronic withdrawals or deposits from international companies/persons do you expect to conduct per month, if any?   0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        // How many domestic wire transfers do you anticipate sending or receiving per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        // How many international wire transfers will you send or receive per month, if any?  0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList3:0']")).click();
        Thread.sleep(2000);
        // Do you intend to use your mobile phone to deposit checks remotely?  No
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1']")).click();
        Thread.sleep(2000);
        // How many domestic electronic withdrawals or deposits do you expect to conduct per month, if any?   0
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:0']")).click();
        Thread.sleep(3000);
        // What channel did the customer use to open the account?    In person - In Branch/ Office
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1']/option[2]")).click();
        Thread.sleep(3000);
        // What is the source of initial funding for this account?
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:multiSelect']/option[3]")).click();
        Thread.sleep(3000);
        // What is the source of ongoing funding for this account?     Withdrawal from another bank
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect']/option[4]")).click();
        Thread.sleep(3000);
        // Click Next
        driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id828']/input[3]")).click();
        Thread.sleep(5000);
        //  Click Next on the Additional Services page
        driver.findElement(By.xpath("//*[@id='addser:frm']/center/input[3]")).click();
        Thread.sleep(5000);
        // SimplyChecking Funding   =   Check
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText']/option[3]")).click();
        Thread.sleep(2000);
        // SimplySavings Funding  = Check and Cash
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText']/option[4]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:1:j_id100:j_id131:j_id133']")).sendKeys("5214");
        Thread.sleep(3000);

        //  Click Next On Account Setup Page
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id144:bottom']/input[3]")).click();
        Thread.sleep(5000);
        // Click "Setup Accounts" Button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id148:j_id206:setupUpAccountsbutton']")).click();
        Thread.sleep(3000);

        //Accept pop-up alert
        driver.switchTo().alert().accept();
        Thread.sleep(8000);

        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:0:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:1:j_id126:2']")).click();
        Thread.sleep(2000);
        //driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:2:j_id126:1']")).click();
        Thread.sleep(3000);

        // Click Finish button
        driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:a:fin']")).click();
        Thread.sleep(5000);
        //driver.close();
    }

    public static String decodeStr(String encodedStr) {
        byte[] decoded = Base64.decodeBase64(encodedStr);

        return new String(decoded);
    }
}

## What is this repo
This is where Selenium scripts, written in JAVA, are being stored so that others can use them or collaborate on them

---

## What is selenium
Selenium is an open source testing framework that allows you to automate web browsers.

you can do cool things like:

1. automatically click buttons
2. send values to fields
3. run an entire process
 
and so much more...



---

## wow this is so great, how do I get started??

Clone this repository. It has basically everything you need to get started including:

1. Selenium libraries
2. Chrome web driver
3. A password encryption method

